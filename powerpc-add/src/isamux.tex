% ISAMUX
% https://bugs.libre-soc.org/show_bug.cgi?id=214

\chapter{ISAMUX}

\paragraph{}

A fixed number of additional (hidden) bits, conceptually a \textbf{namespace},
set by way of a \gls{CSR} or other out-of-band mechanism,
that go directly and non-optionally
into the instruction decode phase, extending (in each implementation) the
opcode length to 16+N, 32+N, 48+N, where N is a hard fixed quantity on
a per-implementor basis.

\paragraph{}

Where the opcode is normally loaded from the location at the PC, the extra
bits, set via a CSR, are mandatorially appended to every instruction: hence why
they are described as "hidden" opcode bits, and as a \textbf{namespace}.

\paragraph{}

The parallels with c++ \textbf{using namespace} are direct and clear.
Alternative conceptual ways to understand this concept include
\textbf{escape-sequencing}.

\paragraph{}

TODO: reserve some bits which permit the namespace \textbf{escape-sequence} to
be relevant for a fixed number of instructions at a time.  Caveat:
allowing such a countdown to cross branch-points is unwise (illegal
instruction?)

\paragraph{}

An example of a pre-existing \textbf{namespace} switch that has been in
prevalent use for several decades (\gls{SPARC} and other architectures):
dynamic runtime selectability of little-endian / big-endian \textbf{meaning}
of instructions by way of a \textbf{mode switch} instruction (of some kind).

\paragraph{}

That \textbf{switch} is in effect a 33rd (hidden) bit that is part of the opcode,
going directly into the mux / decode phase of instruction decode, and
thus qualifies categorically as a \textbf{namespace}.  This proposal both formalises
and generalises that concept.

\section{Hypothetical Format} \label{hypotheticalformat}

\paragraph{}

Note that this is a hypothetical format, yet to be decided, where particular
attention needs to be paid to the fact that there is an \textbf{immediate}
version of CSRRW (with 5 bits of immediate) that could save a lot of space in
binaries.

\begin{verbatim}
   3                   2                   1
|1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0|
|-------------------------------|-------|---------------------|-|
|1 custom custom   custom   custom   custom     | foreignarch |1|
|0 reserved reserved reserved reserved reserved | foreignarch |1|
|custom         | reserved    |           official|B| rvcpage |0|
\end{verbatim}

\paragraph{}

RV Mode

\begin{itemize}
\parskip 0pt
\itemsep 1pt

\item

when bit 0 is 0, \textbf{RV} mode is selected.

\item

in RV mode, bits 1 thru 5 provide up to 16 possible alternative meanings
(namespaces) for 16 Bit opcodes. \textbf{pages} if you will. The top bit
indicates custom meanings. When set to 0, the top bit is for official usage.

\item

Bits 15 thru 23 are reserved.

\item

Bits 24 thru 31 are for custom usage.

\item

bit 6 (\textbf{B}) is \gls{endian}-selection: \gls{LE}/\gls{BE}

\end{itemize}

\paragraph{}

16 bit page examples:

\begin{itemize}
\parskip 0pt
\itemsep 1pt

\item

0b0000 STANDARD (2019) RVC

\item

0b0001 RVCv2

\item

0b0010 RV16

\item

0b0011 RVCv3

\item

...

\item

0b1000 custom 16 bit opcode meanings 1

\item

0b1001 custom 16 bit opcode meanings 2

\item

.....

\end{itemize}

\paragraph{}

Foreign Arch Mode

\begin{itemize}
\parskip 0pt
\itemsep 1pt

\item

when bit 0 (the \gls{LSB}) is 1, \textbf{Foreign arch} mode is selected.
% part of the reason for having LSB here is to avoid glossary ordering problems

\item

Bits 1 thru 7 are a table of foreign arches.

\item

when the \gls{MSB} is 1, this is for custom use.

\item

when the MSB is 0, bits 1 thru 6 are reserved for 64 possible official foreign archs.

\end{itemize}

\paragraph{}

Foreign archs could be (examples):

\begin{itemize}
\parskip 0pt
\itemsep 1pt

\item

0b0000000 x86\_32

\item

0b0000001 x86\_64

\item

0b0000010 MIPS32

\item

0b0000011 MIPS64

\item

....

\item

0b0010000 Java Bytecode

\item

0b0010001 N.E.Other Bytecode

\item

....

\item

0b1000000 custom foreign arch 1

\item

0b1000001 custom foreign arch 2

\item

....

\end{itemize}

\paragraph{}

Note that \textbf{official} foreign archs have a binary value where the MSB is zero,
and custom foreign archs have a binary value where the MSB is 1.

\section{Namespaces are permitted to swap to new state} \label{stateswap}

\paragraph{}

In each privilege level, on a change of ISANS (whether through manual setting
of ISANS or through trap entry or exit changing the ISANS CSR), an
implementation is permitted to completely and arbitrarily switch not only the
instruction set, it is permitted to switch to a new bank of CSRs (or a subset
of the same), and even to switch to a new PC.

\paragraph{}

This to occur immediately and atomically at the point at which the change in ISANS occurs.

\paragraph{}

The most obvious application of this is for Foreign Archs, which may have their
own completely separate PC. Thus, foreign assembly code and \gls{RISCV} assembly code
need not be mixed in the same binary.

\paragraph{}

Further use-cases may be envisaged however great care needs to be taken to not
cause massive complications for JIT emulation, as the RV ISANS is unary encoded
(2\^31 permutations).

\paragraph{}

In addition, the state information of \textbf{all} namespaces has to be saved
and restored on a context-switch (unless the SP is also switched as part of the
state!) which is quite severely burdensome and getting exceptionally complex.

\paragraph{}

Switching \gls{CSR}, \gls{PC} (and potentially \gls{SP}) and other state on a NS change in the
RISCV unary NS therefore needs to be done wisely and responsibly, i.e.
minimised!

\paragraph{}

To be discussed. Context
href=https://groups.google.com/a/groups.riscv.org/d/msg/isa-dev/\\
x-uFZDXiOxY/27QDW5KvBQAJ


\section{Privileged Modes / Traps} \label{privtraps}

\paragraph{}

An additional WLRL CSR per priv-level named \textbf{LAST-ISANS} is required, and
another called \textbf{TRAP-ISANS}
These mirrors the ISANS CSR, and, on a trap, the current ISANS in
that privilege level is atomically
transferred into LAST-ISANS by the hardware, and ISANS in that trap
is set to TRAP-ISANS.  Hardware is \textbf{only then} permitted to modify the \gls{PC} to
begin execution of the trap.

\paragraph{}

On exit from the trap, LAST-ISANS is copied into the ISANS CSR, and
LAST-ISANS is set to TRAP-ISANS. \textbf{Only then} is the hardware permitted
to modify the PC to begin execution where the trap left off.

\paragraph{}

This is identical to how xepc is handled.

\paragraph{}

Note 1: in the case of \gls{SupervisorMode} (context switches in particular),
saving and changing of LAST-ISANS (to and from the stack) must be done
atomically and under the protection of the SIE bit.  Failure to do so
could result in corruption of LAST-ISANS when multiple traps occur in
the same privilege level.

\paragraph{}

Note 2: question - should the trap due to illegal (unsupported) values
written into LAST-ISANS occur when the \textbf{software} writes to LAST-ISANS,
or when the \textbf{trap} (on exit) writes into LAST-ISANS?  this latter seems
fraught: a trap, on exit, causing another trap??

\paragraph{}

Per-privilege-level pseudocode (there exists UISANS, UTRAPISANS, ULASTISANS,
MISANS, MTRAPISANS, MLASTISANS and so on):

\begin{verbatim}
trap_entry()
{
    LAST-ISANS = ISANS // record the old NS
    ISANS = TRAP_ISANS // traps are executed in "trap" NS
}

and trap_exit:

trap_exit():
{
    ISANS = LAST-ISANS
    LAST-ISANS = TRAP_ISANS
}
\end{verbatim}

\section{Alternative RVC 16 Bit Opcode meanings} \label{alternativervc16bitopcodemeanings}

\paragraph{}

Here is appropriate to raise an idea how to cover RVC and future
variants, including RV16.

\paragraph{}

Just as with foreign archs, and you quite rightly highlight above, it
makes absolutely no sense to try to select both RVCv1, v2, v3 and so on,
all simultaneously. An unary bit vector for RVC modes, changing the 16
BIT opcode space meaning, is wasteful and again has us believe that WARL
is the \textbf{solution}.

\paragraph{}

The correct thing to do is, again, just like with foreign archs, to
treat RVCs as a \textbf{binary} namespace selector. Bits 1 thru 3 would give
8 possible completely new alternative meanings, just like how the \gls{Z80}
and the 286 and 386 used to do bank switching.

\paragraph{}

All zeros is clearly reserved for the present RVC. 0b001 for RVCv2. 0b010
for RV16 (look it up) and there should definitely be room reserved here
for custom reencodings of the 16 bit opcode space.

\section{FAQ}\label{faq}

\subsection{Why not have TRAP-ISANS as a vector table, matching mtvec?} \label{trap-isans-vec}

\paragraph{}

Use case to be determined.  Rather than be a global per-priv-level value,
TRAP-ISANS is a table of length exactly equal to the mtvec/utvec/stvec table,
with corresponding entries that specify the assembly-code namespace in which
the trap handler routine is written.

\paragraph{}

Open question: see https://groups.google.com/a/groups.riscv.org/d/msg/isa\-dev/IAhyOqEZoWA/BM0G3J2zBgAJ

\begin{verbatim}
trap_entry(x_cause)
{
    LAST-ISANS = ISANS // record the old NS
    ISANS = TRAP_ISANS_VEC[xcause] // traps are executed in "trap" NS
}

and trap_exit:

trap_exit(x_cause):
{
    ISANS = LAST-ISANS
    LAST-ISANS = TRAP_ISANS_VEC[x_cause]
}
\end{verbatim}

\subsection{Is this like MISA ?} \label{misa}

\paragraph{}

No.

\begin{itemize}
\parskip 0pt
\itemsep 1pt

\item

\gls{MISA}'s space is entirely taken up (and running out).

\item

There is no allocation (provision) for custom extensions.

\item

MISA switches on and off entire extensions: \gls{ISAMUX}/NS may be used to switch
multiple opcodes (present and future), to alternate meanings.

\item

MISA is WARL and is inaccessible from everything but M-Mode (not even readable). 

\end{itemize}

\paragraph{}

MISA is therefore wholly unsuited to U-Mode usage; ISANS is specifically
permitted to be called by userspace to switch (with no stalling) between
namespaces, repeatedly and in quick succession.

\subsection{What happens if this scheme is not adopted? Why is it better than leaving things well alone?} \label{laissezfaire}

\paragraph{}

At the first sign of an emergency non-backwards compatible and unavoidable
change to the \textbf{frozen} RISCV \textbf{official} Standards, the entire RISCV
community is fragmented and divided into two:

\begin{itemize}
\parskip 0pt
\itemsep 1pt

\item

Those vendors that are hardware compatible with the legacy standard.

\item

Those that are compatible with the new standard.

\end{itemize}

\paragraph{}

\textbf{These two communities would be mutually exclusively incompatible}. If
a second emergency occurs, \gls{RISCV} becomes even less tenable.

\paragraph{}

Hardware that wished to be \textbf{compatible} with either flavour would require
\gls{JIT} or offline static binary recompilation. No vendor would willingly
accept this as a condition of the standards divergence in the first place,
locking up decision making to the detriment of RISCV as a whole.

\paragraph{}

By providing a \textbf{safety valve} in the form of a hidden namespace, at least
newer hardware has the option to implement both (or more) variations,
\textbf{and still apply for Certification}.

\paragraph{}

However to also allow \textbf{legacy} hardware to at least be JIT soft
compatible, some very strict rules \textbf{must} be adhered to, that appear at
first sight not to make any sense.

\paragraph{}

It's complicated in other words!

\subsection{Surely it's okay to just tell people to use 48-bit encodings?} \label{use48bit}

\paragraph{}

Short answer: it doesn't help resolve conflicts, and costs hardware and
redesigns to do so.  Softcores in cost-sensitive embedded applications may
even not actually be able to fit the required 48 bit instruction decode engine
into a (small, ICE40) \gls{FPGA}.  48-bit instruction decoding is much more complex
than straight 32-bit decoding, requiring a queue.

\paragraph{}

Second answer: conflicts can still occur in the (unregulated, custom) 48-bit
space, which \textbf{could} be resolved by ISAMUX/ISANS as applied to the \textbf{48} bit
space in exactly the same way.  And the 64-bit space.

\subsection{Why not leave this to individual custom vendors to solve on a case by case basis?} \label{case-by-case}

\paragraph{}

The suggestion was raised that a custom extension vendor could create
their own CSR that selects between conflicting namespaces that resolve
the meaning of the exact same opcode.  This to be done by all and any
vendors, as they see fit, with little to no collaboration or coordination
towards standardisation in any form.

\paragraph{}

The problems with this approach are numerous, when presented to a
worldwide context that the UNIX Platform, in particular, has to face
(where the embedded platform does not)

\paragraph{}

First: lack of coordination, in the proliferation of arbitrary solutions,
has to primarily be borne by \gls{gcc}, \gls{Binutils}, \gls{LLVM} and other compilers.

\paragraph{}

Secondly: CSR space is precious. With each vendor likely needing only one
or two bits to express the namespace collision avoidance, if they make
even a token effort to use worldwide unique CSRs (an effort that would
benefit compiler writers), the CSR register space is quickly exhausted.

\paragraph{}

Thirdly: JIT Emulation of such an unregulated space becomes just as
much hell as it is for compiler writers. In addition, if two vendors
use conflicting CSR addresses, the only sane way to tell the emulator
what to do is to give the emulator a runtime command line argument.

\paragraph{}

Fourthly: with each vendor coming up with their own way of handling
conflicts, not only are the chances of mistakes higher, it is against the
very principles of collaboration and cooperation that save vendors money
on development and ongoing maintenance.  Each custom vendor will have
to maintain their own separate hard fork of the toolchain and software,
which is well known to result in security vulnerabilities.

\paragraph{}

By coordinating and managing the allocation of namespace bits (unary
or binary) the above issues are solved. CSR space is no longer wasted,
compiler and JIT software writers have an easier time, clashes are
avoided, and RISCV is stabilised and has a trustable long term future.

\subsection{ Why ISAMUX / ISANS has to be WLRL and mandatory trap on illegal writes} \label{wlrlmandatorytrap}

\paragraph{}

The namespaces, set by bits in the CSR, are functionally directly
equivalent to c++ namespaces, even down to the use of braces.

\paragraph{}

WARL, by allowing implementors to choose the value, prevents and prohibits
the critical and necessary raising of an exception that would begin the
JIT process in the case of ongoing standards evolution.

\paragraph{}

Without this opportunity, an implementation has no reliable guaranteed way of knowing
when to drop into full JIT mode,
which is the only guaranteed way to distinguish
any given conflicting opcode. It is as if the c++
standard was given a similar optional
opportunity to completely ignore the
\textbf{using namespace} prefix!

\paragraph{}

--

\paragraph{}

Ok so I trust it's now clear why WLRL (thanks Allen) is needed.

\paragraph{}

When Dan raised the WARL concern initially a situation was masked by
the conflict, that if gone unnoticed would jeapordise ISAMUX/ISANS
entirely. Actually, two separate errors. So thank you for raising the
question.

\paragraph{}

The situation arises when foreign archs are to be given their own NS
bit. MIPS is allocated bit 8, x86 bit 9, whilst LE/BE is given bit 0,
RVCv2 bit 1 andso on. All of this potential rather than actual, clearly.

\paragraph{}

Imagine then that software tries to write and set not just bit 8 and
bit 9, it also tries to set bit 0 and 1 as well.

\paragraph{}

This \textbf{IS} on the face of it a legitimate reason to make ISAMUX/ISANS WARL.

\paragraph{}

However it masks a fundamental flaw that has to be addressed, which
brings us back much closer to the original design of 18 months ago,
and it's highlighted thus:

\paragraph{}

x86 and simultaneous RVCv2 modes are total nonsense in the first place!

\paragraph{}

The solution instead is to have a NS bit (bit0) that SPECIFICALLY
determines if the arch is RV or not.  If 0, the rest of the ISAMUX/ISANS
is very specifically RV \textbf{only}, and if 1, the ISAMUX/ISANS is a \textbf{binary}
table of foreign architectures and foreign architectures only.

\paragraph{}

Exactly how many bits are used for the foreign arch table, is to
be determined. 7 bits, one of which is reserved for custom usage,
leaving a whopping 64 possible \textbf{official} foreign instruction sets to
be hardware-supported/JIT-emulated seems to be sufficiently gratuitous,
to me.

\paragraph{}

One of those could even be Java Bytecode!

\paragraph{}

Now, it could \textbf{hypothetically} be argued that the permutation of setting
LE/BE and MIPS for example is desirable. A simple analysis shows this
not to be the case: once in the MIPS foreign NS, it is the MIPS hardware
implementation that should have its own way of setting and managing its
LE/BE mode, because to do otherwise drastically interferes with MIPS
binary compatibility.

\paragraph{}

Thus, it is officially Not Our Problem: only flipping into one foreign
arch at a time makes sense, thus this has to be reflected in the
ISAMUX/ISANS CSR itself, completely side-stepping the (apparent) need
to make the NS CSR WARL (which would not work anyway, as previously
mentioned).

\paragraph{}

So, thank you, again, Dan, for raising this. It would have completely
jeapordised ISAMUX/NS if not spotted.

\paragraph{}

The second issue is: how does any hardware system, whether it support
ISANS or not, and whether any future hardware supports some Namespaces
and, in a transitive fashion, has to support \textbf{more} future namespaces,
through JIT emulation, if this is not planned properly in advance?

\paragraph{}

Let us take the simple case first: a current 2019 RISCV fully compliant
RV64GC UNIX capable system (with mandatory traps on all unsupported CSRs).

\paragraph{}

Fast forward 20 years, there are now 5 ISAMUX/NS unary bits, and 3
foreign arch binary table entries.

\paragraph{}

Such a system is perfectly possible of software JIT emulating ALL of these
options because the write to the (illegal, for that system) ISAMUX/NS
CSR generates the trap that is needed for that system ti begin JIT mode.

\paragraph{}

(This again emphasises exactly why the trap is mandatory).

\paragraph{}

Now let us take the case of a hypothetical system from say 2021 that
implements RVCv2 at the hardware level.

\paragraph{}

Fast forward 20 years: if the CSR were made WARL, that system would be
absolutely screwed. The implementor would be under the false impression
that ignoring setting of \textbf{illegal} bits was acceptable, making the
transition to JIT mode flat-out impossible to detect.

\paragraph{}

When this is considered transitively, considering all future additions to
the NS, and all permutations, it can be logically deduced that there is
a need to reserve a \textbf{full} set of bits in the ISAMUX/NS CSR \textbf{in advance}.

\paragraph{}

i.e. that \textbf{right now}, in the year 2019, the entire ISAMUX/NS CSR cannot
be added to piecemeal, the full 32 (or 64) bits \textbf{has} to be reserved,
and reserved bits set at zero.

\paragraph{}

Furthermore, if any software attempts to write to those reserved bits,
it \textbf{must} be treated just as if those bits were distinct and nonexistent
CSRs, and a trap raised.

\paragraph{}

It makes more sense to consider each NS as having its own completely
separate CSR, which, if it does not exist, clearly it should be obvious
that, as an unsupported CSR, a trap should be raised (and JIT emulation
activated).

\paragraph{}

However given that only the one bit is needed (in RV NS Mode, not
Foreign NS Mode), it would be terribly wasteful of the CSRs to do this,
despite it being technically correct and much easier to understand why
trap raising is so essential (mandatory).

\paragraph{}

This again should emphasise how to mentally get one's head round this
mind-bendingly complex problem space: think of each NS bit as its own
totally separate CSR that every implementor is free and clear to implement
(or leave to JIT Emulation) as they see fit.

\paragraph{}

Only then does the mandatory need to trap on write really start to hit
home, as does the need to preallocate a full set of reserved zero values
in the RV ISAMUX/NS.

\paragraph{}

Lastly, I \textbf{think} it's ok to only reserve say 32 bits, and, in 50 years
time if that genuinely is not enough, start the process all over again
with a new CSR.  ISAMUX2/NS2.

\paragraph{}

Subdivision of the RV NS (support for RVCv3/4/5/RV16 without wasting
precious CSR bits) best left for discussion another time, the above is
a heck of a lot to absorb, already.

\subsection{Why WARL will not work and why WLRL is required}

\paragraph{}

WARL requires a follow-up read of the CSR to ascertain what heuristic
the hardware \textbf{might} have applied, and if that procedure is followed in
this proposal, performance even on hardware would be severely compromised.

\paragraph{}

In addition when switching to foreign architectures, the switch has to
be done atomically and guaranteed to occur.

\paragraph{}

In the case of JIT emulation, the WARL \textbf{detection} code will be in an
assembly language that is alien to hardware.

\paragraph{}

Support for both assembly languages immediately after the CSR write
is clearly impossible, this leaves no other option but to have the CSR
be WLRL (on all platforms) and for traps to be mandatory (on the UNIX
Platform).

\subsection{Is it strictly necessary for foreign archs to switch back?} \label{foreignswitch}

\paragraph{}

No, because LAST-ISANS handles the setting and unsetting of the ISANS CSR
in a completely transparent fashion as far as the foreign arch is concerned.
Supervisor or Hypervisor traps take care of the context switch in a way
that the user mode (or guest) need not be aware of, in any way.

\paragraph{}

Thus, in e.g. Hypervisor Mode, the foreign guest arch has no knowledge
or need to know that the hypervisor is flipping back to RV at the time of
a trap.

\paragraph{}

Note however that this is \textbf{not} the same as the foreign arch executing
\textbf{foreign} traps!  Foreign architecture trap and interrupt handling mechanisms
are \textbf{out of scope} of this document and MUST be handled by the foreign
architecture implementation in a completely transparent fashion that in
no way interacts or interferes with this proposal.

\subsection{Can we have dynamic declaration and runtime declaration of capabilities?} \label{dynamic}

\paragraph{}

Answer: don't know (yet).  Quoted from Rogier:

\begin{quote}
"A \gls{SoC} may have several devices that one may want to directly control
with custom instructions. If independent vendors use the same opcodes you
either have to change the encodings for every different chip (not very
nice for software) or you can give the device an ID which is defined in
some device tree or something like that and use that."
\end{quote}

\paragraph{}

Dynamic detection wasn't originally planned: static
compilation was envisaged to solve the need, with a table of
mvendorid-marchid-isamux/isans being maintained inside gcc / binutils /
llvm (or separate library?) that, like the Linux kernel ARCH table,
requires a world-wide atomic \textbf{git commit} to add globally-unique
registered entries that map functionality to actual namespaces.

\paragraph{}

where that goes wrong is if there is ever a pair (or more) of vendors
that use the exact same custom feature that maps to different opcodes,
a statically-compiled binary has no hope of executing natively on both
systems.

\paragraph{}

at that point: yes, something akin to device-tree would be needed.

\section{Open Questions}\label{open-questions}

\paragraph{}

This section from a post by Rogier Bruisse
\href{http://hands.com/~lkcl/gmail_re_isadev_isamux.html}{http://hands.com/~lkcl/gmail\_re\_isadev\_isamux.html}

\subsection{is the ISANS CSR a 32 or XLEN bit value?} \label{isans-32-or-xlen}

\paragraph{}

This is partly answered in another FAQ above: if 32 bits is not enough
for a full suite of official, custom-with-atomic-registration and custom-without
then a second CSR group (ISANS2) may be added at a future date (10-20 years
hence).

\paragraph{}

32 bits would not inconvenience RV32, and implementors wishing to
make significant altnernative modifications to opcodes in the RV32 ISA space
could do so without the burden of having to support a split 32/LO 32/HI
CSR across two locations.

\subsection{Is the ISANS a flat number space or should some bits be reserved for use as flags?}

\paragraph{}

See 16-bit RV namespace "page" concept, above.  Some bits have to be unary
(multiple simultaneous features such as LE/BE in one bit, and augmented
Floating-point rounding / clipping in another), whilst others definitely
need to be binary (the most obvious one being \textbf{paging} in the space currently
occupied by RVC).

\subsection{Should the ISANS space be partitioned between reserved, custom with registration guaranteed non clashing, custom, very likely non clashing?}

\paragraph{}

Yes. Format TBD.

\subsection{Should only compiler visible/generated constant setting with CSRRWI and/or using a clearly recognisable LI/LUI be accommodated or should dynamic setting be accommodated as well?}

\paragraph{}

This is almost certainly a software design issue, not so much a hardware
issue.

\subsection{How should the ISANS be (re)stored in a trap and in context switch?}

\paragraph{}

See section above on privilege mode: LAST-ISANS has been introduced that
mirrors (x)CAUSE and (x)EPC pretty much exactly.  Context switches change
uepc just before exit from the trap, in order to change the user-mode PC
to switch to a new process, and ulast-isans can - must - be treated in
exactly the same way.  When the context switch sets ulast-isans (and uepc),
the hardware flips both ulast-isans into uisans and uepc into pc (atomically):
both the new NS and the new PC activate immediately, on return to usermode.

\paragraph{}

Quite simple.

\subsection{Should the mechanism accommodate "foreign ISA's" and if so how does one restore the ISA.}

\paragraph{}

See section above on LAST-ISANS.  With the introduction of LAST-ISANS, the
change is entirely transparent, and handled by the Supervisor (or Hypervisor)
trap, in a fashion that the foreign ISA need not even know of the existence
of ISANS.  At all.

\subsection{Where is the default ISA stored and what  is responsible for what it is after}

\paragraph{}

Options:
\begin{itemize}
\parskip 0pt
\itemsep 1pt

\item

start up

\item

starting a program

\item

calling into a dynamically linked library

\item

taking a trap

\item

changing privilege levels

\end{itemize}

\paragraph{}

These first four are entirely at the discretion of (and the
responsibility of) the software.  There is precedent for most of these
having been implemented, historically, at some point, in relation to
LE/BE mode CSRs in other hardware (MIPSEL vs MIPS distros for example).

\paragraph{}

Traps are responsible for saving LAST-ISANS on the stack, exactly as they
are also responsible for saving other context-sensitive information such
as the registers and xEPC.

\paragraph{}

The hardware is responsible for atomically switching out ISANS into the
relevant xLAST-ISANS (and back again on exit).  See Privileged Traps,
above.

\subsection{If the ISANS is just bits of an instruction that are to be prefixed by the cpu, can those bits contain immediates? Register numbers?}

\paragraph{}

The concept of a CSR containing an immediate makes no sense.  The concept
of a CSR containing a register number, the contents of which would, presumably,
be inserted into the NS, would immediately make that register a permanent
and irrevocably reserved register that could not be utilised for any other
purpose.

\paragraph{}

This is what the CSRs are supposed to be for!

\paragraph{}

It would be better just to have a second CSR - ISANS2 - potentially even ISANS3
in 60+ years time, rather than try to use a GPR for the purposes for which CSRs
are intended.

\subsection{How does the system indicate a namespace is not recognised? Does it trap or can/must a recoverable mechanism be provided?}

\paragraph{}

It doesn't "indicate" that a namespace is not recognised.  WLRL fields only
hold supported values.  If the hardware cannot hold the value, a trap
\textbf{MUST} be thrown (in the UNIX platform), and at that point it becomes the
responsibility of software to deal with it.

\subsection{What are the security implications? Can some ISA namespaces be set by user space?}

\paragraph{}

Of course they can.  It becomes the responsibility of the Supervisor Mode
(the kernel) to treat ISANS in a fashion orthogonal to the PC.  If the OS
is not capable of properly context-switching securely by setting the right
PC, it's not going to be capable of properly looking after changes to ISANS.

\subsection{Does the validity of an ISA namespace depend on privilege level? If so how?}

\paragraph{}

The question does not exactly make sense, and may need a re-reading of the
section on how Privilege Modes, above.  In RISC-V, privilege modes do not
actually change very much state of the system: the absolute minimum changes
are made (swapped out) - xEPC, xSTATUS and so on - and the privilege mode
is expected to handle the context switching (or other actions) itself.

\paragraph{}

ISANS - through LAST-ISANS - is absolutely no different.  The trap and the
kernel (Supervisor or Hypervisor) are provided the \textbf{mechanism} by which
ISA Namespace \textbf{may} be set: it is up to the software to use that mechanism
correctly, just as the software is expected to use the mechanisms provided
to correctly implement context-switching by saving and restoring register
files, the PC, and other state.  The NS effectively becomes just another
part of that state.
